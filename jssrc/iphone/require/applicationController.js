define({
    AS_AppEvents_cf22c8464e6441bda8252a37b9471ed6: function AS_AppEvents_cf22c8464e6441bda8252a37b9471ed6(eventobject) {
        var self = this;
        setAllCallbacks();
    },
    appInit: function(params) {
        skinsInit();
        kony.application.setCheckBoxSelectionImageAlignment(constants.CHECKBOX_SELECTION_IMAGE_ALIGNMENT_RIGHT);
        kony.application.setDefaultTextboxPadding(false);
        kony.application.setRespectImageSizeForImageWidgetAlignment(true);
        kony.mvc.registry.add("flxSampleRowTemplate", "flxSampleRowTemplate", "flxSampleRowTemplateController");
        kony.mvc.registry.add("flxSectionHeaderTemplate", "flxSectionHeaderTemplate", "flxSectionHeaderTemplateController");
        kony.mvc.registry.add("Form1", "Form1", "Form1Controller");
        setAppBehaviors();
    },
    postAppInitCallBack: function(eventObj) {
        return applicationController.AS_AppEvents_cf22c8464e6441bda8252a37b9471ed6(eventObj);
    },
    appmenuseq: function() {
        new kony.mvc.Navigation("Form1").navigate();
    }
});