var KMSPROP = {};
var pushCallbacks = {
    onsuccessfulregistration: regSuccessCallback,
    onfailureregistration: regFailureCallback,
    onlinenotification: onlinePushNotificationCallback,
    offlinenotification: offlinePushNotificationCallback,
    onsuccessfulderegistration: unregSuccessCallback,
    onfailurederegistration: unregFailureCallback
};
/*
 * @function setAllCallbacks
 * this function will set the call backs for push notification 
 */
function setAllCallbacks() {
    try {
        kony.push.setCallbacks(pushCallbacks);
    } catch (err) {
        alert("KMS Module" + JSON.stringify(err));
    }
}
/*
 * @function registerKMS
 * this function will register the divice for GMS console and APS console using
 * register API
 */
function registerKMS() {
    KMSPROP.osType = "iphone";
    configRegister = [0, 1, 2];
    //this will check whether the device is al
    //if (kony.store.getItem("isRegisteredForKMS") === undefined || kony.store.getItem("isRegisteredForKMS") === "" || kony.store.getItem("isRegisteredForKMS") === null) {
    kony.push.register(configRegister);
    //}
}
/*
 * @function regSuccessCallback
 * This function is registration success callback on successful registration of APS or GCM
 * this function is used to register to kony messaging  for push notification
 */
function regSuccessCallback(regId) {
    //try {
    //kony.store.setItem("isRegisteredForKMS", "true");
    KMSPROP.deviceId = kony.os.deviceInfo().deviceid; // device id
    KMSPROP.device_rec = kony.os.deviceInfo().deviceid; // unique key
    var client = kony.sdk.getCurrentInstance();
    var messagingSvc = client.getMessagingService();
    messagingSvc.register(KMSPROP.osType, KMSPROP.deviceId, regId, KMSPROP.device_rec, successCallbackForSubscribe, failureCallbackForSubscribe);
    alert("4");

    function successCallbackForSubscribe(res) {
        alert(JSON.stringify(res));
    }

    function failureCallbackForSubscribe(err) {
        kony.store.removeItem("isRegisteredForKMS");
        alert(JSON.stringify(err));
        alert("Subscription Failed.");
    }
    //} catch (err) {
    //	alert("KMS Module" + JSON.stringify(err));
    //}
}
/*
 * @function regFailureCallback
 * This function is registration failure callback on failure registration of APS or GCM
 */
function regFailureCallback(errormsg) {
    alert(errormsg);
    alert("Registration Failed ");
}
/*
 * @function onlinePushNotificationCallback
 * This function is the callback for online push notification
 */
function onlinePushNotificationCallback(msg) {
    alert("message incoming");
    try {
        alert("push message received" + JSON.stringify(msg));
    } catch (err) {
        alert("KMS Module" + JSON.stringify(err));
    }
}
/*
 * @function offlinePushNotificationCallback
 * This function is the callback for offline push notification
 */
function offlinePushNotificationCallback(msg) {
    alert("message incoming offline");
    try {
        alert("push message received" + JSON.stringify(msg));
    } catch (err) {
        alert("KMS Module" + JSON.stringify(err));
    }
}

function unregSuccessCallback() {
    alert("Unregisterd Succesfully!!");
}

function unregFailureCallback(errormsg) {
    alert(" Unregistration Failed!!" + errormsg);
}