function skinsInit() {
    defBtnFocus = "defBtnFocus";
    defBtnNormal = "defBtnNormal";
    sknLblDescription = "sknLblDescription";
    sknLblRowHeading = "sknLblRowHeading";
    sknLblStrip = "sknLblStrip";
    sknLblTimeStamp = "sknLblTimeStamp";
    sknSampleRowTemplate = "sknSampleRowTemplate";
    sknSampleSectionHeaderTemplate = "sknSampleSectionHeaderTemplate";
    sknSectionHeaderLabelSkin = "sknSectionHeaderLabelSkin";
    slDynamicNotificationForm = "slDynamicNotificationForm";
    slForm = "slForm";
    slPopup = "slPopup";
    slStaticNotificationForm = "slStaticNotificationForm";
    slTitleBar = "slTitleBar";
    slWatchForm = "slWatchForm";
};