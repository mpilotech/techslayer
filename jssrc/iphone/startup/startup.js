//startup.js
var globalhttpheaders = {};
var appConfig = {
    appId: "TechSlayer",
    appName: "TechSlayer",
    appVersion: "1.0.0",
    isturlbase: "https://mpilotech-dev.konycloud.com/services",
    isDebug: true,
    isMFApp: true,
    appKey: "fe05ce3c58a54bcf8be33437510cb900",
    appSecret: "b7628824cd1d19d88ecd6639463ce75f",
    serviceUrl: "https://100000158.auth.konycloud.com/appconfig",
    svcDoc: {
        "identity_meta": {
            "GetAuthToken": {
                "success_url": "allow_any"
            }
        },
        "app_version": "1.0",
        "messagingsvc": {
            "appId": "2d73ba68-5104-4bc0-9cae-2ff2e69e3a36",
            "url": "https://mpilotech-dev.messaging.konycloud.com/api/v1"
        },
        "baseId": "1079f600-ece0-4496-b83b-845bd8aa6d26",
        "app_default_version": "1.0",
        "login": [{
            "provider_type": "userstore",
            "forgot_pswd_submit_userid": "https://100000158.auth.konycloud.com/forgot_password/submit_userid/",
            "reset_pswd": "https://100000158.auth.konycloud.com/forgot_password/reset_password/",
            "alias": "Example",
            "type": "basic",
            "prov": "Example",
            "url": "https://100000158.auth.konycloud.com"
        }, {
            "mandatory_fields": [],
            "provider_type": "oauth2",
            "alias": "GetAuthToken",
            "type": "basic",
            "prov": "GetAuthToken",
            "url": "https://100000158.auth.konycloud.com"
        }],
        "services_meta": {
            "ImageStorage": {
                "offline": false,
                "metadata_url": "https://mpilotech-dev.konycloud.com/services/metadata/v1/ImageStorage",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://mpilotech-dev.konycloud.com/services/data/v1/ImageStorage"
            },
            "test55": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mpilotech-dev.konycloud.com/services/test55"
            },
            "ServiceList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mpilotech-dev.konycloud.com/services/ServiceList"
            }
        },
        "selflink": "https://100000158.auth.konycloud.com/appconfig",
        "integsvc": {
            "_internal_logout": "https://mpilotech-dev.konycloud.com/services/IST",
            "test55": "https://mpilotech-dev.konycloud.com/services/test55",
            "ServiceList": "https://mpilotech-dev.konycloud.com/services/ServiceList"
        },
        "service_doc_etag": "0000016ED5917108",
        "appId": "2d73ba68-5104-4bc0-9cae-2ff2e69e3a36",
        "identity_features": {
            "reporting_params_header_allowed": true
        },
        "name": "TechSlayer",
        "reportingsvc": {
            "session": "https://mpilotech-dev.konycloud.com/services/IST",
            "custom": "https://mpilotech-dev.konycloud.com/services/CMS"
        }
    },
    runtimeAppVersion: "1.0",
    eventTypes: ["FormEntry", "Error", "Crash"],
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        isMVC: true,
        APILevel: 8400
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    applicationController = require("applicationController");
    callAppMenu();
    kony.application.setApplicationInitializationEvents({
        init: applicationController.appInit,
        postappinit: applicationController.postAppInitCallBack,
        showstartupform: function() {
            new kony.mvc.Navigation("Form1").navigate();
        }
    });
};

function loadResources() {
    globalhttpheaders = {};
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//This is the entry point for the application.When Locale comes,Local API call will be the entry point.
loadResources();
debugger;